import Vue from 'vue'
import Router from 'vue-router'
import TrHome from './views/Home.vue'
import TrDomainDetail from './views/DomainDetail.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: TrHome
    },
    {
      path: '/detail/:domain',
      name: 'detail',
      component: TrDomainDetail,
      props: true // Se adiciona para permitir enviar propiedades al componente TrDomainDetail
    }
  ]
})
