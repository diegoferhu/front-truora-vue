const urlBase = "http://localhost:3000/",
  domainService = {}

domainService.search = function (istDomain) {
  return fetch(`${urlBase}domains/${istDomain}`)
    .then(response => response.json())
}
domainService.history = function () {
  return fetch(`${urlBase}domains/history`)
    .then(response => response.json())
}
export default domainService